-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2017 at 08:40 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `reminder`
--

-- --------------------------------------------------------

--
-- Table structure for table `cv_remind`
--

CREATE TABLE `cv_remind` (
  `id` int(11) NOT NULL,
  `ten_cv` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ten_nv` text COLLATE utf8_unicode_ci NOT NULL,
  `cc_mail` text COLLATE utf8_unicode_ci,
  `tgth` date NOT NULL,
  `loai_cv` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `uu_tien` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv_remind`
--

INSERT INTO `cv_remind` (`id`, `ten_cv`, `ten_nv`, `cc_mail`, `tgth`, `loai_cv`, `uu_tien`, `status`) VALUES
(2, 'abc', 'Nguyễn C', NULL, '2017-04-18', 'Bảo Trì Định Kỳ', 'Trung Bình', 0),
(3, 'blog.vinahost', '1;3;6;7', '', '2017-04-22', 'Bảo Trì Định Kỳ', 'Trung Bình', 0),
(5, 'Blog.vinahost.vn', '1;2;6;', '', '2017-04-29', 'Báo Cáo Định Kỳ', 'Cao', 0),
(7, 'Blog.vnh.vn', '1;2', '', '2017-04-20', 'Báo Cáo Định Kỳ', 'Trung Bình', 0),
(9, 'Blog.vnh.mod.vn', '1;3;4', '', '2017-04-21', 'Báo Cáo Định Kỳ', 'Cao', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nv`
--

CREATE TABLE `nv` (
  `id` int(11) NOT NULL,
  `ten_nv` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nv`
--

INSERT INTO `nv` (`id`, `ten_nv`, `mail`) VALUES
(1, 'Nguyễn A', 'minhdh@vinahost.vn'),
(2, 'Nguyễn B', 'lovelykiller3000@gmail.com'),
(3, 'Nguyễn C', 'c@gmail.com'),
(4, 'Nguyễn D', 'd@gmail.com'),
(5, 'Nguyễn E', ''),
(6, 'Nguyễn F', ''),
(7, 'Nguyễn G', ''),
(8, 'Nguyễn H', ''),
(9, 'Nguyễn I', ''),
(10, 'Nguyễn K', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cv_remind`
--
ALTER TABLE `cv_remind`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nv`
--
ALTER TABLE `nv`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cv_remind`
--
ALTER TABLE `cv_remind`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `nv`
--
ALTER TABLE `nv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
