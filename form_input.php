
<?php
    $kn = mysqli_connect('localhost', 'root', '', 'reminder');

    if(!$kn)
    {
        echo "ket noi den db that bai";
    }

    mysqli_query($kn,'SET NAMES utf8');
?>


<!DOCTYPE html>
<html>
    <head>
        <title>Form Nhập Công Việc</title>
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="css/custom.css">
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body id="body">

    <div id="title">Nhập Thông Tin Cần Nhắc Nhở</div>
    <div id="form" class="container">
        <form class="form-horizontal" method="post" action="#">
            <div class="form-group" style="position: relative">
                <label for="stt" class="col-sm-2 control-label">Số Thứ Tự</label>
                <?php
                    $stt = "SELECT MAX(id) as max_id FROM cv_remind";
                    $query_stt = mysqli_query($kn, $stt);
                    $arr = mysqli_fetch_array($query_stt);
                    if(empty($arr))
                    {
                        ?>
                        <div style="width: 30px;margin-left: 250px;margin-top: 6px; color: red"><?php echo '#1'; ?></div>
                        <?php
                    }
                    else
                    {
                        ?>
                        <div style="width: 30px;margin-left: 250px;margin-top: 6px; color: red"><?php echo '#' . intval($arr["max_id"] + 1); ?></div>
                        <?php
                    }
                ?>
            </div>

            <div class="form-group">
                <label for="ten_cv" class="col-sm-2 control-label label_pos">Tên Công Việc</label>
                <div class="col-sm-10 box_size">
                    <input type="text" class="form-control" name="ten_cv" placeholder="Nhập vào tên công việc...">
                </div>
            </div>

            <div class="form-group">
                <label for="ten_nv" class="col-sm-2 control-label label_pos">Tên Người Nhắc Nhở</label>
                <div class="col-sm-10 box_size checkbox_list">
                    <?php
                        $query_active = "SELECT * FROM nv";
                        $nv = mysqli_query($kn, $query_active);
                        while ($i = mysqli_fetch_array($nv))
                        {
                            ?>
                            <input type="checkbox" name="<?php echo $i['id']; ?>" value="<?php echo $i['id'];
                            ?>"><?php echo $i['ten_nv']; ?> <br />
                            <?php
                        }
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label for="cc_mail" class="col-sm-2 control-label label_pos">Danh Sách Mail Cc</label>
                <div class="col-sm-10 box_size">
                    <textarea class="form-control" name="cc_mail" rows="3"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="tgth" class="col-sm-2 control-label label_pos">Thời Gian Thực Hiện</label>
                <div class="col-sm-10 box_size">
                    <input type="date" class="form-control" name="tgth">
                </div>
            </div>

            <div class="form-group">
                <label for="loai_cv" class="col-sm-2 control-label label_pos">Loại Công Việc</label>
                <div class="radio radio_pos">
                    <label>
                        <input type="radio" name="loai_cv" value="Bảo Trì Định Kỳ" checked>Bảo Trì Định Kỳ
                    </label>
                </div>
                <div class="radio radio_pos">
                    <label>
                        <input type="radio" name="loai_cv" value="Báo Cáo Định Kỳ">Báo Cáo Định Kỳ
                    </label>
                </div>
                <div class="radio radio_pos">
                    <label>
                        <input type="radio" name="loai_cv" value="Khác">Khác
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label for="loai_cv" class="col-sm-2 control-label label_pos">Mức Độ Ưu Tiên</label>
                <div class="radio radio_pos">
                    <label>
                        <input type="radio" name="uu_tien" value="Cao" checked>Cao
                    </label>
                </div>
                <div class="radio radio_pos">
                    <label>
                        <input type="radio" name="uu_tien" value="Trung Bình">Trung Bình
                    </label>
                </div>
                <div class="radio radio_pos">
                    <label>
                        <input type="radio" name="uu_tien" value="Thấp">Thấp
                    </label>
                </div>
            </div>

            <button type="submit" class="btn btn-primary" style="margin-left: 100px; margin-top: 20px">Lưu</button>

        </form>
    </div>

    </body>
</html>

<?php
    if($_SERVER['REQUEST_METHOD'] == "POST")
    {


        $nv_nn = "";
        $nv = "SELECT id FROM nv";
        $query_nv_post = mysqli_query($kn, $nv);
        $dem = 0;
        while ($i = mysqli_fetch_array($query_nv_post))
        {
            $tmp = $i["id"];
            if(!empty($_POST[$tmp]))
            {
                if($dem == 0)
                {
                    $nv_nn .= $tmp;
                }
                else
                {
                    $nv_nn .= ';' . $tmp;
                }
            }
            $dem++;
        }

        $ten_cv = $_POST['ten_cv'];
        $cc_mail = "support@vinahost.vn;" .$_POST['cc_mail'];
        $tg = $_POST['tgth'];
        $loai_cv = $_POST['loai_cv'];
        $utien = $_POST['uu_tien'];


        $insert = "INSERT INTO cv_remind (ten_cv, ten_nv, cc_mail, tgth, loai_cv, uu_tien)
                          VALUES ('".$ten_cv."', '".$nv_nn."', '".$cc_mail."', '".$tg."', '".$loai_cv."', '".$utien."')";

        $query_insert = mysqli_query($kn, $insert);

    }
?>